import axios from "axios";

const baseUrl = "https://api.openweathermap.org/data/2.5/";

//types
const WEATHER = {
  GET_FORECAST_WEATHER: "tania-ibarra/WEATHER/GET_FORECAST_WEATHER",
  GET_DATA_WEATHER: "tania-ibarra/WEATHER/GET_DATA_WEATHER",
  GET_CURRENT_WEATHER: "tania-ibarra/WEATHER/GET_CURRENT_WEATHER",
  GET_FORECAST_WEATHER_SUCCESS:
    "tania-ibarra/WEATHER/GET_FORECAST_WEATHER_SUCCESS",
  GET_CURRENT_WEATHER_SUCCESS:
    "tania-ibarra/WEATHER/GET_CURRENT_WEATHER_SUCCESS",
  GET_DATA_WEATHER_SUCCESS: "tania-ibarra/WEATHER/GET_DATA_WEATHER_SUCCESS",
  GET_FORECAST_WEATHER_ERROR: "tania-ibarra/WEATHER/GET_FORECAST_WEATHER_ERROR",
  GET_CURRENT_WEATHER_ERROR: "tania-ibarra/WEATHER/GET_CURRENT_WEATHER_ERROR",
  GET_DATA_WEATHER_ERROR: "tania-ibarra/WEATHER/GET_DATA_WEATHER_ERROR",
};

const initialState = {
  data: [],
  forecastData: [],
  currentData: [],
  loading: false,
  weatherDataError: [],
};

export default function weather(state = initialState, action) {
  switch (action.type) {
    case WEATHER.GET_FORECAST_WEATHER:
    case WEATHER.GET_DATA_WEATHER:
    case WEATHER.GET_CURRENT_WEATHER:
      return {
        ...state,
        loading: true,
      };
    case WEATHER.GET_FORECAST_WEATHER_SUCCESS:
      return {
        ...state,
        loading: false,
        forecastData: action.payload,
      };
    case WEATHER.GET_CURRENT_WEATHER_SUCCESS:
      return {
        ...state,
        loading: false,
        currentData: action.payload,
      };
    case WEATHER.GET_DATA_WEATHER_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload,
      };
    case WEATHER.GET_FORECAST_WEATHER_ERROR:
      return {
        ...state,
        loading: false,
        weatherDataError: action.payload,
      };
    case WEATHER.GET_CURRENT_WEATHER_ERROR:
      return {
        ...state,
        loading: false,
        weatherDataError: action.payload,
      };
    case WEATHER.GET_DATA_WEATHER_ERROR:
      return {
        ...state,
        loading: false,
        weatherDataError: action.payload,
      };
    default:
      return state;
  }
}

//actions
function actionWeather(actionType, reqName) {
  return {
    type: actionType,
    reqName,
  };
}

function actionGetWeather(actionType, reqName, payload) {
  return {
    type: actionType,
    reqName,
    payload,
  };
}

//dispatchers

// consulta el clima actual al dia de hoy
export const fetchCurrentWeather = (lat, lng) => async (dispatch) => {
  dispatch(actionWeather(WEATHER.GET_CURRENT_WEATHER, "getCurrentWeather"));
  try {
    const { data } = await axios.get(
      `${baseUrl}weather?lat=${lat}&lon=${lng}&units=metric&appid=${process.env.REACT_APP_WEATHER_KEY}&lang={sp}`
    );

    dispatch(
      actionGetWeather(
        WEATHER.GET_CURRENT_WEATHER_SUCCESS,
        "getCurrentDataWeather",
        data
      )
    );
    return data;
  } catch (e) {
    dispatch(
      actionGetWeather(
        WEATHER.GET_CURRENT_WEATHER_ERROR,
        "getCurrentDataWeatherError",
        e.response
      )
    );
    throw e;
  }
};

// consulta el clima en los proximos 5 dias
export const fetchForecastWeather = (lat, lng) => async (dispatch) => {
  dispatch(actionWeather(WEATHER.GET_FORECAST_WEATHER, "getForecastWeather"));
  try {
    const { data } = await axios.get(
      `${baseUrl}uvi/forecast?cnt=4&lat=${lat}&lon=${lng}&units=metric&appid=${process.env.REACT_APP_WEATHER_KEY}&lang={sp}`
    );
    dispatch(
      actionGetWeather(
        WEATHER.GET_FORECAST_WEATHER_SUCCESS,
        "getForecastDataWeather",
        data
      )
    );

    return data;
  } catch (e) {
    dispatch(
      actionGetWeather(
        WEATHER.GET_FORECAST_WEATHER_ERROR,
        "getForecastDataWeatherError",
        e.response
      )
    );
    throw e;
  }
};

// consulta el clima de acuerdo a la ciudad
export const fetchWeather = (payload) => async (dispatch) => {
  dispatch(actionWeather(WEATHER.GET_DATA_WEATHER, "getWeather"));
  try {
    const { data } = await axios.get(
      `${baseUrl}weather?q=${payload}&appid=${process.env.REACT_APP_WEATHER_KEY}&units=metric&lang={sp}`
    );

    dispatch(
      actionGetWeather(WEATHER.GET_DATA_WEATHER_SUCCESS, "getDataWeather", data)
    );
    return data;
  } catch (e) {
    dispatch(
      actionGetWeather(
        WEATHER.GET_DATA_WEATHER_ERROR,
        "getDataWeatherError",
        e.response.status
      )
    );
    throw e;
  }
};

//selectors
export const getForecastWeather = (state) => state.weather.forecastData;
export const getDataWeather = (state) => state.weather.data;
export const getCurrentWeather = (state) => state.weather.currentData;
export const getLoader = (state) => state.weather.loading;
export const getStatusError = (state) => state.weather.weatherDataError;
