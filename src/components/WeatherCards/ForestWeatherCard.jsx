import React from 'react'
import styles from './WeatherCard.module.scss'
import Spinner from '../Spinner/Spinner'

const ForestWeatherCard = ({ forecastWeather, loadingCurrentData, loadingForecastData }) => {
    const days = [
        'Domingo',
        'Lunes',
        'Martes',
        'Miércoles',
        'Jueves',
        'Viernes',
        'Sábado',
    ];

    return (
        <>
            <h2 className={styles.weatherForestCard}>Pronóstico extendido</h2>
            <div className={styles.weatherForestCard_gridContainer}>
                {forecastWeather?.map((item, idx) => {
                    const day = new Date(item?.date_iso).getDay();
                    const nameDay = days[day];
                    return (

                        <div className={styles.weatherForestCard_gridContainer__content} key={idx}>
                            {loadingCurrentData || loadingForecastData ? <Spinner /> :
                                <>
                                    <p>{nameDay}</p>
                                    <p>{Math.ceil(Number(item?.value))} °C</p>
                                </>}
                        </div>);
                })}
            </div>
        </>
    );

}

export default ForestWeatherCard;