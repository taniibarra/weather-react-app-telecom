import React, { Suspense } from 'react'
import Spinner from '../Spinner/Spinner';
import styles from './WeatherCard.module.scss'


const WeatherCard = ({ currentWeather, loadingCurrentData, loadingForecastData }) => {
    const emptyData = Object.keys(currentWeather).length === 0;

    return (
        <Suspense fallback={<Spinner />}>
            {!emptyData &&
                <div className={styles.weatherCard_container}>
                    <div className={styles.weatherCard_container__content}>
                        {loadingCurrentData || loadingForecastData ? <Spinner /> : <><h2>Hoy</h2><p>{currentWeather?.weather[0]?.main}</p>
                            <p className={styles.weatherCard_container__content_nameCity}>{currentWeather?.name}</p>
                            <p className={styles.weatherCard_container__content_temp}>{Math.ceil(Number(currentWeather?.main?.temp))} °C</p>
                            <div className={styles.weatherCard_container__content_temp_minMax}>
                                <span >Máx: {Math.ceil(Number(currentWeather?.main?.temp_max))}°C</span>
                                <span>Min: {Math.ceil(Number(currentWeather?.main?.temp_min))}°C</span>
                            </div>
                            <img src={`https://openweathermap.org/img/wn/${currentWeather?.weather[0]?.icon}@2x.png`} alt='icon' />
                        </>}
                    </div>
                </div>}
        </Suspense>

    );
}

export default WeatherCard;