import React, { Suspense, useEffect, useState } from 'react'
import styles from './CityCard.module.scss'
import Spinner from '../Spinner/Spinner';

const CityCard = ({ dataCity, handleCity, loadingCurrentData, loadingForecastData, dataWeatherLength }) => {

    const [title, setTitle] = useState('')

    useEffect(() => {
        if (!dataWeatherLength) {
            setTitle('Cargando ciudades...')
        } else {
            setTitle('Seleccioná otra ciudad para conocer su pronóstico extendido')
        }
    }, [dataWeatherLength])

    return (
        <Suspense>
            <h2 className={styles.title}>{title}</h2>
            <div className={styles.cityCard_container}>
                {dataCity.map((c, idx) => {
                    return (
                        <div className={styles.cityCard_container__content} key={idx}>
                            {loadingCurrentData || loadingForecastData ? <Spinner /> :
                                <button className={styles.cityCard_container__content_button} onClick={() => handleCity(idx)}>
                                    <p>{c.name}</p>
                                    <p>{c.temp}°C</p>
                                </button>}
                        </div>);
                }
                )}
            </div>
        </Suspense>
    );
}

export default CityCard;