import React from 'react'
import styles from './Error.module.scss'

const ErrorPage = () => {
    return (
        <div className={styles.error_page}>
            <div className={styles.error_page__content}>
                <p>Ha ocurrido un error! <br /> Por favor volvé a intentar mas tarde</p>
            </div>
        </div>
    );
}

export default ErrorPage;