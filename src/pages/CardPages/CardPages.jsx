import React from 'react'
import WeatherCard from '../../components/WeatherCards/WeatherCard'
import ForestWeatherCard from '../../components/WeatherCards/ForestWeatherCard'
import CityCard from '../../components/WeatherCards/CityCard'

const CardPages = ({ currentWeather, loadingCurrentData, loadingForecastData, forecastWeather, dataCity, handleCity, dataWeatherLength }) => {
    return (
        <>
            <WeatherCard currentWeather={currentWeather} loadingCurrentData={loadingCurrentData} loadingForecastData={loadingForecastData} />
            <ForestWeatherCard forecastWeather={forecastWeather} loadingCurrentData={loadingCurrentData} loadingForecastData={loadingForecastData} />
            <CityCard dataCity={dataCity} handleCity={handleCity} loadingCurrentData={loadingCurrentData} loadingForecastData={loadingForecastData} dataWeatherLength={dataWeatherLength} />
        </>

    );
}

export default CardPages;