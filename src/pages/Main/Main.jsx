import React, { useEffect, useMemo, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Header from '../../components/Header/Header';
import { getUserLocation } from '../../helpers/getUserLocation';
import { fetchForecastWeather, fetchWeather, getForecastWeather, fetchCurrentWeather, getCurrentWeather, getLoader, getStatusError } from '../../redux/ducks/weather'
import CardPages from '../CardPages/CardPages'
import ErrorPage from '../ErrorPage/Error';

const Main = () => {
    const dispatch = useDispatch();

    // setea la ubicacion actual
    const [currentLocation, setCurrentLocation] = useState({
        lng: 0,
        lat: 0
    })
    const [dataCity, setDataCity] = useState([{ name: '', temp: 0, coord: {} }]);
    const [newCoord, setNewCoord] = useState({})

    const forecastWeather = useSelector((state) =>
        getForecastWeather(state, 'getForecastDataWeather')
    );
    const currentWeather = useSelector((state) =>
        getCurrentWeather(state, 'getCurrentWeather')
    );

    const statusError = useSelector((state) => getStatusError(state, 'getForecastDataWeatherError'))


    const dataWeatherLength = Object.keys(dataCity).length > 4;

    const loadingCurrentData = useSelector((state) => getLoader(state, 'getCurrentWeather'))
    const loadingForecastData = useSelector((state) => getLoader(state, 'getForecastWeather'))


    const emptyNewCoord = Object.keys(newCoord).length === 0;

    const cities = useMemo(() => [
        { cityName: 'Rosario' },
        { cityName: 'Buenos Aires' },
        { cityName: 'Mendoza' },
        { cityName: 'Bariloche' },
        { cityName: 'Cordoba' },
    ], [])


    // Obtiene las coordenadas de la ciudad seleccionada
    const handleCity = (indexParent) => {
        dataCity.map((d, idx) => {
            if (indexParent === idx)
                return (
                    setNewCoord(d.coord)
                )
        }
        )
    }

    if (!navigator.geolocation) {
        alert('El navegador no tiene opción de geolocalización');
        throw new Error('El navegador no tiene opción de geolocalización')
    }

    // obtiene la ubicacion acutal a traves del helper
    useEffect(() => {
        getUserLocation().then(l => setCurrentLocation({
            lng: emptyNewCoord ? l[0] : newCoord.lon,
            lat: emptyNewCoord ? l[1] : newCoord.lat
        }))
    }, [emptyNewCoord, newCoord]);


    useEffect(() => {
        dispatch(fetchForecastWeather(currentLocation.lat, currentLocation.lng))
        dispatch(fetchCurrentWeather(currentLocation.lat, currentLocation.lng))
    }, [currentLocation, dispatch])

    // aca itero las ciudades y realizo la consulta a la api para obtener sus respectivas temperaturas
    useEffect(() => {
        let nameCityArray = [];

        cities.map((c) => dispatch(fetchWeather(c.cityName)).then((data) => {
            nameCityArray.push({ name: data.name, temp: data?.main?.temp, coord: data.coord })
        }))

        setDataCity(nameCityArray)

    }, [cities, dispatch])

    return (
        <>
            <Header />
            {statusError === 401 || statusError === 429 ?
                <ErrorPage />
                :
                <CardPages
                    currentWeather={currentWeather}
                    loadingCurrentData={loadingCurrentData}
                    loadingForecastData={loadingForecastData}
                    forecastWeather={forecastWeather}
                    dataCity={dataCity}
                    handleCity={handleCity}
                    dataWeatherLength={dataWeatherLength}
                />}

        </>
    );
}

export default Main;