
export const getUserLocation = async () => {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(
            ({ coords }) => {
                resolve([coords.longitude, coords.latitude])
            },
            (error) => {
                alert('No se pudo obtener la ubicacion actual');
                console.warn(error)
                reject()
            }
        )
    })
}