# Challenge Weather App

## Tania Ibarra

Proyecto hecho con [Create React App](https://github.com/facebook/create-react-app).

## Prueba tecnica de Frontend

La aplicación consta de 1 componente principal donde se visualiza el estado del clima en la ciudad actual, y para los próximos 5 días de la misma.

Además, consta de 5 cards seleccionables con diferentes ciudades para ver su clima actual y el pronóstico extendido a 5 días.

Se implemento un helper para obtener la localizacion actual del usuario.

Se utilizo redux para el manejo de estados de la API.
La api utilizada fue [Open Weather Map](https://openweathermap.org/api)

Se utilizó suspense para mejorar la perfomance en la carga de componentes.

Con respecto a los estilos se utilizo sass modules.

El diseño es full-responsive.

## Available Scripts

Clonar el repo.

Instalar dependencias con npm install.

Crear archivo .env en root con el nombre `REACT_APP_WEATHER_KEY` e insertar la key de autorizacion de la api.

Inicializar con npm start.

Corre en puerto [http://localhost:3000].

## DEPLOY

Proyecto deployado en Vercel [Weather React App](https://weather-react-app-telecom.vercel.app/)

**Si la app no funciona o muestra el cartel de ERROR por favor clonar el proyecto, e insertar su propia API KEY de OpenWeather**
